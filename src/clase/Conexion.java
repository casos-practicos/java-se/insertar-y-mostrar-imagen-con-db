/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clase;
import java.sql.*;



/**
 *
 * @author AldomarMoralesCarlos
 */
public class Conexion {
    private static Conexion conex;
    public static Conexion getInstance(){
        if(conex==null){
            conex=new Conexion();
        }
        return conex;
    }
    public Connection getConnection() throws ClassNotFoundException, SQLException{
        Class.forName("com.mysql.jdbc.Driver");
        return DriverManager.getConnection("jdbc:mysql://localhost:3306/imagenes","root","aldomar");
    }
    public static void main(String[] args){
        try{
            System.out.println(getInstance().getConnection());
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    
}
