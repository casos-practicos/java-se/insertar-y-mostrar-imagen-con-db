/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clase;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

/**
 *
 * @author Aldo
 */
public class Controlador {

    public void guardar(String ruta, String nombre) {
        FileInputStream fi = null;
        try {
            Connection connection = Conexion.getInstance().getConnection();
            String sql = "insert into usuario (nombre,foto) values(?,?)";
            //capturamos la ruta de la imagen
            File file = new File(ruta);
            //lee la imagen en tipo bytes
            fi = new FileInputStream(file);
            //Abrimos la consulta
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setString(1, nombre);
            statement.setBinaryStream(2, fi);
            statement.executeUpdate();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error al guardar");
        }
    }

    public void LimpiarTabla(DefaultTableModel md) {
        while (md.getRowCount() > 0) {
            md.removeRow(0);
        }
    }

    public void MostrarEnTabla(String consulta, JTable tabla, DefaultTableModel md, int ndatos) {
        Object[] fila = new Object[ndatos];
        int i = 0;
        tabla.setDefaultRenderer(Object.class, new TablaImagen());
        try {
            LimpiarTabla(md);
            Connection connection = Conexion.getInstance().getConnection();
            PreparedStatement statement = connection.prepareStatement(consulta);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                while (i < ndatos - 1) {
                    fila[i] = rs.getObject(i + 1);
                    i++;
                }
                Blob blob = rs.getBlob(ndatos);
                byte[] data = blob.getBytes(1, (int) blob.length());
                BufferedImage img = null;
                try {
                    img = ImageIO.read(new ByteArrayInputStream(data));
                } catch (Exception e) {
                    JOptionPane.showMessageDialog(null, "No se puede leer la imagen");
                }
                ImageIcon icono = new ImageIcon(img.getScaledInstance(50, 60, BufferedImage.SCALE_DEFAULT));
                fila[ndatos - 1] = new JLabel(icono);
                md.addRow(fila);
                i = 0;
            }
            //para ordenar la tabla ---->con un click en la cabecera
            TableRowSorter<TableModel> ordena = new TableRowSorter<TableModel>(md);
            tabla.setRowSorter(ordena);
            //agregamos el modelo a la tabla
            tabla.setModel(md);
            //alto de la fila
            tabla.setRowHeight(64);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Mostrar en la tabla");
        }
    }

    public String CapturarId(String sql, String tex) {
        String id = "";
        try {
            Connection connection = Conexion.getInstance().getConnection();
            PreparedStatement statement = connection.prepareCall(sql + tex + "'");
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                id = rs.getString(1);
            }
            connection.close();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Capturar id");
        }
        return id;
    }
}
